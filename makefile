CC = gcc
CFLAGS = -std=c99 -g
CXX = g++
CXXFLAGS = -std=c++11 -g

EXE = web_shell
OBJS = web_shell.o httplib.o

# platform issue
MAKE = make
UNAME = $(shell uname)
ifeq ($(UNAME), FreeBSD)
    MAKE = gmake
endif

all: $(EXE)

clean: 
	rm -f $(EXE) $(OBJS)

$(EXE): $(OBJS)
	$(CXX) -o $@ $(CXXFLAGS) $^

$(OBJS): %.o: %.cpp
	$(CXX) -o $@ $(CXXFLAGS) -c $<

.PHONY: all clean
