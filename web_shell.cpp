#include <iostream>
#include <map>

#include <unistd.h>

#include "httplib.h"

/*
 * web_shell.cpp: a toy web shell for embedded system homework.
 *
 * steps:
 * 1. get HTTP request, fetch post parameter(command)
 * 2. fork and exec command, use pipe recieve output
 * 3. fill output into html template, and return HTTP response
 */

int main(int argc, char *argv[]){
    // 1. get HTTP request, fetch post parameter(command)
    std::map<std::string, std::string> query_parameters;
    query_parameters = http::http_get_parameters();

    // 2. fork and exec command, use pipe recieve output
    // query_parameters["command"] => sl_arg
    // std::string output;
    // real_run(sl_arg, output);

    // 3. fill output into html template, and return HTTP response
    return 0;
}

void real_run(char** sl_arg, std::string& output){ 
    int pipe_out[2]; 
    if(pipe(pipe_out) == 1){ // 建立通道
        puts("Pipe build err!!\n"); 
        exit(1); 
    } 
    pid_t pid; 
    if((pid=fork())<0){ // 建立 child process
        puts("fork build err!!\n"); 
        exit(1); 
    } 
    else if(pid==0){ // child process 該做的事情 
        close(pipe_out[0]); 
        dup2(pipe_out[1],STDOUT_FILENO); // 將 standard out 導至 pipe_out 
        close(pipe_out[1]); 
        execvp(sl_arg[0],sl_arg); // 執行程式 
        std::cout<<"wrong cmd!!"<<std::endl; // 一般來說只有 execvp 失敗才會跳至這裡 
        exit(1); 
    } 
    else{ // parent process 做的事情 
        close(pipe_out[1]); 
        char tt[1024]; 
        int k1=read(pipe_out[0],tt,1023); // 讀取資料 
        tt[k1]='\0'; 

        output=std::string(tt);
        close(pipe_out[0]); 
    } 
}
