#include <vector>

#include "httplib.h"

namespace http{

    std::map<std::string, std::string> http_get_parameters(){
        /* 
         * get HTTP GET parameters from environment variables, defined by CGI protocol.
         * return key-value pair parameters.
         */

        std::string query_string = getenv("QUERY_STRING");
        std::vector<std::string> parameter_vector;

        while( 1 ){
            /* bad smell string split, but it works */
            std::size_t spliter = query_string.find_first_of('&');

            if( spliter == std::string::npos ){
                std::string parameter = query_string;
                parameter_vector.push_back(parameter);
                break;
            }

            std::string parameter = query_string.substr(0, spliter);
            parameter_vector.push_back(parameter);
            query_string = query_string.substr(spliter+1, std::string::npos);
        }

        std::map<std::string, std::string> query_parameters;
        for( const auto& parameter: parameter_vector ){
            std::size_t spliter = parameter.find_first_of('=');
            if( spliter == std::string::npos ){
                continue;
            }
            std::string key = parameter.substr(0, spliter);
            std::string value = parameter.substr(spliter+1, std::string::npos);
            if( value.empty() )
                continue;

            query_parameters[key] = value;
        }

        return query_parameters;
    }

}
